= generate-release-notes
:toc:
:toc-title: Table of Contents

== Overview

`generate-release-notes` is a Python script that gets incomplete Asana tasks in the "Done" section in the Curriculum Dev Kanban and writes them to a Markdown file.

== Prerequisites

* Python 3 — if you don't have the latest version on your machine, you can download it from link:https://www.python.org/downloads/[Python's website].
* pip — pip is included with your Python download.

== Installation

Before you can use this script, you need to install all required packages.

The following command installs the packages according to the configuration file in `requirements.txt`:

[source,bash]
----
pip install -r requirements.txt
----

=== Authentication

This script first checks to make sure users are authorized to make requests to Asana's API. You'll need to set several environment variables in your `.env` file:

* `ASANA_TOKEN`: your Personal Access Token
* `PROJECT_ID`: the GID for the Curriculum Dev Kanban
* `SECTION_ID`: the GID for the "Done" section in the Kanban
* `CUSTOM_FIELD_ID`: the GID for the "Task type" field
* `TAG_ID`: the GID for the 2.4 tag

There's some logic that checks the validity of your Personal Access Token and constructs an Asana client if you're authorized.

=== Initializing a Markdown file

This script uses the Python package `mdutils` to create a Markdown file, and set its filename and top-level header.

[source, python]
----
release_notes = MdUtils(file_name='release-notes')
release_notes.new_header(level=1, title='Release notes draft')
----

=== Getting tasks with a 2.4 tag

A function called `get_two_point_four_tasks()` gets all tasks in the "Done" section with a tag of 2.4.

It first calls the `get_tasks_for_section` function on the Asana client. The function takes in the `section_id` (gid for the "Done" section) and optional fields to search for:

* `name`: the task's name
* `custom_fields`: all custom fields for a task, like "task type" or "size"
* `completed`: boolean indicating if the task is complete
* `tags`: all tags for a task, like "LMS", "2.4", or "Added After Sprint"

If the task has a tag of 2.4, it is added to a `list`.

=== Writing tasks to a Markdown file

The function `write_tasks()` writes all tasks with a task type of "New Content", "Major Enhancement", "Minor Enhancement", or "Bug Fix" to a Markdown file. It takes in three parameters: a task type to search for, a list of tasks, and a `release_notes` object.

It first checks to see if the task type matches the task type the function is searching for. If it does, the function constructs a string that consists of the task's name and URL and adds it to a `list`.

[source, python]
----
if task['custom_fields'][2]['display_value'] == task_type:
    items = [task['name'] + ' ' + '[[Week #, Day #]]' +'(https://app.asana.com/0/' + self.project_id + '/' + task['gid'] + ')']
----

Next, there's some logic that checks to make sure that the `items` list isn't empty before writing the task type as a heading to the Markdown file. Then, the function calls the `mdutils` function `new_list()` on the `release_notes` object. `new_list()` takes in a list of strings and writes each list item as a bulleted item to a Markdown file.

=== Creating a Markdown file

The last part of the script calls several functions to initialize the Markdown file, search through all tasks in the "Done" column and write tasks with a matching task type to the file, and then create the file.

[source, python]
----
release_notes = generate_notes.initialize_release_notes()
tasks = generate_notes.get_two_point_four_tasks()

release_notes = generate_notes.write_tasks('New Content', tasks, release_notes)
release_notes = generate_notes.write_tasks('Major Enhancement', tasks, release_notes)
release_notes = generate_notes.write_tasks('Minor Enhancement', tasks, release_notes)
release_notes = generate_notes.write_tasks('Bug Fix', tasks, release_notes)

release_notes.create_md_file()
----

_A note from Deanna: right now, you have to run this script in the `generate-release-notes` repository and then copy the content from the newly created `release-notes` file. I couldn't figure out how to make this script runnable outside of the repository._

